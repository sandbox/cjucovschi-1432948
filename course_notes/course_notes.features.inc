<?php
/**
 * @file
 * course_notes.features.inc
 */

/**
 * Implements hook_node_info().
 */
function course_notes_node_info() {
  $items = array(
    'course_notes' => array(
      'name' => t('Course Notes'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
